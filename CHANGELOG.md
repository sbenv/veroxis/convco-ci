# Changelog

## [v0.1.2](https://gitlab.com/sbenv/veroxis/convco-ci/compare/v0.1.1...HEAD) (2023-07-02)

### Fixes

* downgrade crate tracing
([3cd9e92](https://gitlab.com/sbenv/veroxis/convco-ci/commit/3cd9e929baf8387065780b1e6259a54dfe92d95c))

### Other

* **deps:** update registry.gitlab.com/sbenv/veroxis/images/kaniko docker tag
to v1.12.1
([a33ba52](https://gitlab.com/sbenv/veroxis/convco-ci/commit/a33ba5209b48a60fb19c1871046c6e3476630aaf))
* **deps:** update rust crate clap to 4.3.9
([6dbb6dc](https://gitlab.com/sbenv/veroxis/convco-ci/commit/6dbb6dca65487eff7654bd8e0690095bbff4b616))
* **deps:** update registry.gitlab.com/sbenv/veroxis/images/kaniko docker tag
to v1.12.0
([535863f](https://gitlab.com/sbenv/veroxis/convco-ci/commit/535863fdf82c400fd7cc6648e717a8eda53e83d6))
* **deps:** update rust crate tokio to 1.29.0
([aeb0c0f](https://gitlab.com/sbenv/veroxis/convco-ci/commit/aeb0c0ff67fcbdcdfdbd453f728a0c1410c36e14))
* **deps:** update rust crate clap to 4.3.8
([58dfea1](https://gitlab.com/sbenv/veroxis/convco-ci/commit/58dfea120855cf055f7687f5a8e093cc285e58de))
* **deps:** update rust crate clap to 4.3.6
([3784095](https://gitlab.com/sbenv/veroxis/convco-ci/commit/37840956fc23673a4ed46718aaaf9aff848aeeb7))
* **deps:** update rust crate clap to 4.3.5
([efcddd2](https://gitlab.com/sbenv/veroxis/convco-ci/commit/efcddd22b2582570df081216eb5a162eddb3a18a))
* **deps:** update registry.gitlab.com/sbenv/veroxis/images/alpine docker tag
to v3.18.2
([917727b](https://gitlab.com/sbenv/veroxis/convco-ci/commit/917727b1eabe02d7328e6095ba55e17755c9e807))
* **deps:** update rust crate clap to 4.3.4
([31abe95](https://gitlab.com/sbenv/veroxis/convco-ci/commit/31abe95ff7e662c73d45644e52193d3e193a711a))
* **deps:** update rust crate clap to 4.3.3
([181a263](https://gitlab.com/sbenv/veroxis/convco-ci/commit/181a263e800d945ca06233440642ef9d8c71b7de))
* **deps:** update registry.gitlab.com/sbenv/veroxis/images/kaniko docker tag
to v1.11.0
([fb49b98](https://gitlab.com/sbenv/veroxis/convco-ci/commit/fb49b98ae93370672cab5735556363ac4dd0f6ab))
* **deps:** update rust crate clap to 4.3.2
([56052c0](https://gitlab.com/sbenv/veroxis/convco-ci/commit/56052c0428e2f151d3ad72fb24e28e34c92305fb))
* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker
tag to v1.70.0
([af43b09](https://gitlab.com/sbenv/veroxis/convco-ci/commit/af43b091e10c5f38aa1f05117a99ccc3acb21496))
* **deps:** update rust crate clap to 4.3.1
([2c80808](https://gitlab.com/sbenv/veroxis/convco-ci/commit/2c808087f59b86122a1bbc3bd1f4e0c161151791))
* **deps:** update registry.gitlab.com/sbenv/veroxis/images/podman docker tag
to v4.5.1
([7975bd2](https://gitlab.com/sbenv/veroxis/convco-ci/commit/7975bd2ea498187d849afee9beec831dfc8fe9f4))
* **deps:** update rust crate tokio to 1.28.2
([d903829](https://gitlab.com/sbenv/veroxis/convco-ci/commit/d903829f1999d6ba78d49a2f1b79d6e8675c20d6))
* **deps:** update registry.gitlab.com/sbenv/veroxis/images/kaniko docker tag
to v1.10.0
([51d137c](https://gitlab.com/sbenv/veroxis/convco-ci/commit/51d137c86f9db80be0a7e356380f2055db4f4f3c))
* **deps:** update rust crate clap to 4.3.0
([af7ed36](https://gitlab.com/sbenv/veroxis/convco-ci/commit/af7ed360fead7e01c34f7d1d120cfbc7b0df023c))
* **deps:** update rust crate tokio to 1.28.1
([430f2d2](https://gitlab.com/sbenv/veroxis/convco-ci/commit/430f2d27c8f0ec72421cc41aa67ae58591d25336))
* **deps:** update registry.gitlab.com/sbenv/veroxis/images/alpine docker tag
to v3.18.0
([995af5a](https://gitlab.com/sbenv/veroxis/convco-ci/commit/995af5a9fad5499d777d4eeb89975daf079133b6))
* **deps:** update rust crate clap to 4.2.7
([d7157a0](https://gitlab.com/sbenv/veroxis/convco-ci/commit/d7157a0df8ec79205f61b08c477d28083418c305))
* **deps:** update rust crate anyhow to 1.0.71
([ce3e9a6](https://gitlab.com/sbenv/veroxis/convco-ci/commit/ce3e9a645c3a49a4add5d6f1b0a49f50b7a9e371))
* **deps:** lock file maintenance
([7c4e9c0](https://gitlab.com/sbenv/veroxis/convco-ci/commit/7c4e9c0bc1fc5a89586ea2b370c1bd754059ecfa))
* **deps:** update rust crate clap to 4.2.5
([bef11dc](https://gitlab.com/sbenv/veroxis/convco-ci/commit/bef11dc97b048794b6aa5bed55a57b226a2a74f0))
* **deps:** lock file maintenance
([caf62af](https://gitlab.com/sbenv/veroxis/convco-ci/commit/caf62af4658e7ea4dcee3e449e52b8aea4590768))
* **deps:** update rust crate tokio to 1.28.0
([14a1033](https://gitlab.com/sbenv/veroxis/convco-ci/commit/14a1033e47ea9f25df85e5d1401ece39c2c3bde4))
* **deps:** update rust crate tracing to 0.1.38
([8f2dd8d](https://gitlab.com/sbenv/veroxis/convco-ci/commit/8f2dd8d268097ed279a5034c3eed7f6ed1a229de))
* **deps:** lock file maintenance
([ac0aa50](https://gitlab.com/sbenv/veroxis/convco-ci/commit/ac0aa50806492b1abfa5fbe82523005233c2ba80))
* **deps:** lock file maintenance
([04f5f09](https://gitlab.com/sbenv/veroxis/convco-ci/commit/04f5f097a1a7dab708847125fc1caf8bbbb38945))
* **deps:** lock file maintenance
([2098377](https://gitlab.com/sbenv/veroxis/convco-ci/commit/209837712bad48f9b7418c9750b7fd2e4b1c3e7a))
* **deps:** update rust crate tracing-subscriber to 0.3.17
([276de3a](https://gitlab.com/sbenv/veroxis/convco-ci/commit/276de3a9cbc7b728f307ad470f848463fc6c709a))
* **deps:** update registry.gitlab.com/sbenv/veroxis/images/sdks/rust docker
tag to v1.69.0
([06e4c9d](https://gitlab.com/sbenv/veroxis/convco-ci/commit/06e4c9de85964db4c1ad680cd6dd83763af5ae1b))
* **deps:** lock file maintenance
([551e992](https://gitlab.com/sbenv/veroxis/convco-ci/commit/551e992a049ca21597bc18ecd0d54699bef2c3dc))
* **deps:** lock file maintenance
([da66ee5](https://gitlab.com/sbenv/veroxis/convco-ci/commit/da66ee5030c448415c3cce2426562e9fa5a6317a))
* **deps:** lock file maintenance
([d465a4c](https://gitlab.com/sbenv/veroxis/convco-ci/commit/d465a4c6f58c751ce4499630bd8c703b670be37d))
* **deps:** update rust crate clap to 4.2.4
([ce8709c](https://gitlab.com/sbenv/veroxis/convco-ci/commit/ce8709c4f37ddb5bb5303c9509acffb31397d3a0))
* **deps:** lock file maintenance
([b624ca7](https://gitlab.com/sbenv/veroxis/convco-ci/commit/b624ca78f68bb39dbe027108b87786080de894b8))
* **deps:** update rust crate clap to 4.2.3
([0c59a99](https://gitlab.com/sbenv/veroxis/convco-ci/commit/0c59a9906474883ebabe789f2cfe91e6abe3415b))
* **deps:** lock file maintenance
([90954fe](https://gitlab.com/sbenv/veroxis/convco-ci/commit/90954fe3edade4fcb20b5d6f8b1e2587dca86346))
* **deps:** update registry.gitlab.com/sbenv/veroxis/images/podman docker tag
to v4.5.0
([533fa94](https://gitlab.com/sbenv/veroxis/convco-ci/commit/533fa9436a4dd8cc78a6883332e356d820d08b87))
* **deps:** lock file maintenance
([8384a7c](https://gitlab.com/sbenv/veroxis/convco-ci/commit/8384a7c6073803209ecfd8644d53c6260c42d961))
* **deps:** update rust crate clap to 4.2.2
([680098c](https://gitlab.com/sbenv/veroxis/convco-ci/commit/680098c7379de371df4221024e0d8fae09497e08))

### v0.1.1 (2023-04-15)

#### Features

* add convco templates
([3618d04](https://gitlab.com/sbenv/veroxis/convco-ci/commit/3618d04e0864415c9284df4f9aeb5fbf77f7f352))
* print version info
([3ca9f3f](https://gitlab.com/sbenv/veroxis/convco-ci/commit/3ca9f3fe9a8729075116ad94fc8c6cce47b756c2))
* initial commit
([3c3ac95](https://gitlab.com/sbenv/veroxis/convco-ci/commit/3c3ac9556fedf5bc87ba226b37be9d8c6dbe75e1))

#### Other

* **release:** v0.1.1
([1ec3f76](https://gitlab.com/sbenv/veroxis/convco-ci/commit/1ec3f76d553cfce99ef490af4e75461ecaf2dc86))
* add missing need
([751fc04](https://gitlab.com/sbenv/veroxis/convco-ci/commit/751fc044a89513b29573c967b5951eadaaa9418a))
* **release:** v0.1.0
([667cfc8](https://gitlab.com/sbenv/veroxis/convco-ci/commit/667cfc83faf2aaef30f068fe0cbee1b4121c3d59))
* create release builds
([6b44eb5](https://gitlab.com/sbenv/veroxis/convco-ci/commit/6b44eb5d9f4f758c6ad710d8a3f96b35fb8bc1b0))
