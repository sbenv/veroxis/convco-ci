FROM registry.gitlab.com/sbenv/veroxis/images/convco:0.5.2 as convco

FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2

COPY "convco-ci-bin" "/usr/local/bin/convco-ci"
COPY --from=convco "/usr/local/bin/convco" "/usr/local/bin/convco"

ENV GIT_AUTHOR_NAME="convco-ci"
ENV GIT_AUTHOR_EMAIL="convco-ci"
ENV GIT_COMMITTER_NAME="convco-ci"
ENV GIT_COMMITTER_EMAIL="convco-ci"

ENV GIT_AUTH_NAME="convco-ci"
ENV GIT_AUTH_PASS=""

RUN apk add --no-cache "git" && \
    git config --global --add safe.directory "*"
