use anyhow::anyhow;
use anyhow::Result;
use clap::Parser;

#[derive(Debug, Parser)]
pub struct CredentialHelperArgs {
    /// ENV variable name from which the username should be loaded
    #[arg(long, default_value = "GIT_AUTH_NAME")]
    pub user_name: String,

    /// ENV variable name from which the password should be loaded
    #[arg(long, default_value = "GIT_AUTH_PASS")]
    pub user_password: String,

    /// commands which git uses to query for credentials
    #[command(subcommand)]
    pub sub_command: GitCredentialHelperActions,
}

#[derive(Parser, Debug)]
#[command()]
pub enum GitCredentialHelperActions {
    /// configure git to this program as the credential helper
    Install,
    /// used by git
    Get,
    /// used by git
    Store,
    /// used by git
    Erase,
}

pub async fn run(args: CredentialHelperArgs) -> Result<()> {
    match args.sub_command {
        GitCredentialHelperActions::Get => {
            print_credentials_from_env(args.user_name.as_str(), args.user_password.as_str()).await?
        }
        GitCredentialHelperActions::Store => {}
        GitCredentialHelperActions::Erase => {}
        GitCredentialHelperActions::Install => {
            set_git_credential_helper(args.user_name.as_str(), args.user_password.as_str()).await?
        }
    };
    Ok(())
}

async fn set_git_credential_helper(user_name_env: &str, user_password_env: &str) -> Result<()> {
    let script_path = std::env::current_exe()?;
    let script_path = script_path
        .to_str()
        .ok_or(anyhow!("failed to convert script path to UTF-8"))?
        .to_owned();
    let credential_helper_cmd = format!("{script_path} git credential-helper --user-name={user_name_env} --user-password={user_password_env}");
    let mut cmd = tokio::process::Command::new("git");
    cmd.args([
        "config",
        "--global",
        "credential.helper",
        credential_helper_cmd.as_str(),
    ]);
    let result = cmd.output().await?;
    if !result.status.success() {
        return Err(anyhow!("failed to set credential helper"));
    }
    Ok(())
}

async fn print_credentials_from_env(user_name_env: &str, user_password_env: &str) -> Result<()> {
    let user_name = get_env_not_empty(user_name_env)?;
    let user_password = get_env_not_empty(user_password_env)?;
    print!("username={user_name}\npassword={user_password}\n");
    Ok(())
}

fn get_env_not_empty(env_name: &str) -> Result<String> {
    match std::env::var(env_name) {
        Ok(value) => match value.is_empty() {
            true => Err(anyhow!("ENV variable {env_name} is empty")),
            false => Ok(value),
        },
        Err(_) => Err(anyhow!("ENV variable {env_name} is not set")),
    }
}
