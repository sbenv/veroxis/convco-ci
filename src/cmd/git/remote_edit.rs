use anyhow::anyhow;
use anyhow::Result;
use clap::Parser;

#[derive(Debug, Parser)]
pub struct RemoteEditArgs {
    /// the remote which should be edited
    #[arg(long, default_value = "origin")]
    pub remote: String,

    /// commands which git uses to query for credentials
    #[command(subcommand)]
    pub sub_command: RemoteEditActions,
}

#[derive(Parser, Debug, Clone, Copy)]
#[command()]
pub enum RemoteEditActions {
    /// delete credentials of the remote
    UnsetCredentials,
}

pub async fn run(args: RemoteEditArgs) -> Result<()> {
    match args.sub_command {
        RemoteEditActions::UnsetCredentials => unset_credentials(args).await?,
    };
    Ok(())
}

async fn unset_credentials(args: RemoteEditArgs) -> Result<()> {
    let mut cmd = tokio::process::Command::new("git");
    cmd.args(["remote", "get-url", args.remote.as_str()]);
    let output = cmd.output().await?;
    if !output.status.success() {
        return Err(anyhow!("failed to read remote url"));
    }
    let remote_url = String::from_utf8(output.stdout)?;
    let remote_url = git_url_parse::GitUrl::parse(remote_url.as_str())
        .unwrap()
        .trim_auth()
        .to_string();
    let mut cmd = tokio::process::Command::new("git");
    cmd.args([
        "remote",
        "set-url",
        args.remote.as_str(),
        remote_url.as_str(),
    ]);
    let output = cmd.output().await?;
    if !output.status.success() {
        return Err(anyhow!("failed to set remote url"));
    }
    Ok(())
}
