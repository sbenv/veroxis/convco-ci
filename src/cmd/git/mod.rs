pub mod credential_helper;
pub mod remote_edit;

use anyhow::Result;
use clap::Parser;
use credential_helper::CredentialHelperArgs;
use remote_edit::RemoteEditArgs;

#[derive(Debug, Parser)]
pub struct GitArgs {
    #[command(subcommand)]
    pub sub_command: GitCommands,
}

#[derive(Parser, Debug)]
#[command(name = "git_commands")]
pub enum GitCommands {
    /// a git credential helper for loading ENV variables
    #[command(name = "credential-helper")]
    CredentialHelper(CredentialHelperArgs),

    /// edit the git remote
    #[command(name = "remote-edit")]
    RemoteEdit(RemoteEditArgs),
}

pub async fn run(args: GitArgs) -> Result<()> {
    match args.sub_command {
        GitCommands::CredentialHelper(args) => credential_helper::run(args).await?,
        GitCommands::RemoteEdit(args) => remote_edit::run(args).await?,
    }
    Ok(())
}
