pub mod parse_build_version;

use anyhow::Result;
use clap::Parser;

use self::parse_build_version::ParseBuildVersionArgs;

#[derive(Debug, Parser)]
pub struct GitlabArgs {
    #[command(subcommand)]
    pub sub_command: GitlabCommands,
}

#[derive(Parser, Debug)]
#[command()]
pub enum GitlabCommands {
    /// try to parse the version to build within the current pipeline based on if it is a commit or a branch pipeline
    ///
    /// - uses the git tag to parse the version in tags
    /// - uses convco to parse the version for branch builds
    #[command()]
    ParseBuildVersion(ParseBuildVersionArgs),
}

pub async fn run(args: GitlabArgs) -> Result<()> {
    match args.sub_command {
        GitlabCommands::ParseBuildVersion(args) => parse_build_version::run(args).await?,
    }
    Ok(())
}
