use anyhow::anyhow;
use anyhow::Result;
use clap::Parser;

use crate::cmd::semver::normalize::get_normalized_semver_version;

#[derive(Debug, Parser)]
pub struct ParseBuildVersionArgs {}

pub async fn run(_args: ParseBuildVersionArgs) -> Result<()> {
    let mut current_branch: Option<String> = None;
    let mut current_tag: Option<String> = None;

    if let Ok(branch) = std::env::var("CI_COMMIT_BRANCH") {
        current_branch = Some(branch);
    }

    if let Ok(branch) = std::env::var("CI_MERGE_REQUEST_SOURCE_BRANCH_NAME") {
        current_branch = Some(branch);
    }

    if let Ok(tag) = std::env::var("CI_COMMIT_TAG") {
        current_tag = Some(tag);
    }

    if let Some(tag) = current_tag {
        tracing::info!("pipeline type: tag");
        tracing::info!("parsing version from git tag: `{tag}`");
        let version = get_normalized_semver_version(tag.trim())?;
        tracing::info!("detected version: `{version}`");
        println!("{version}");
        return Ok(());
    }

    if current_branch.is_some() {
        tracing::info!("pipeline type: branch");
        tracing::info!("parsing version using: `convco version --bump`");
        let mut cmd = tokio::process::Command::new("convco");
        cmd.args(["version", "--bump"]);
        let result = cmd.output().await?;
        if !result.status.success() {
            return Err(anyhow!("failed to query version using convco"));
        }
        let version = String::from_utf8(result.stdout)?;
        let version = version.trim().to_owned();
        tracing::info!("detected version: `{version}`");
        println!("{}", version.trim());
        return Ok(());
    }

    Err(anyhow!(
        "failed to detect if this is run in a branch or tag pipeline"
    ))
}
