pub mod git;
pub mod gitlab;
pub mod semver;

use clap::Parser;
use git::GitArgs;

use self::gitlab::GitlabArgs;
use self::semver::SemverArgs;

#[derive(Debug, Parser)]
#[command(name = "convco-ci", version)]
pub struct ConvcoCiArguments {
    #[command(subcommand)]
    pub sub_command: ConvcoCiCommands,
}

#[derive(Parser, Debug)]
#[command()]
pub enum ConvcoCiCommands {
    /// commands to use git in ci
    #[command()]
    Git(GitArgs),

    /// commands to use git in ci
    #[command()]
    Semver(SemverArgs),

    /// commands to use git in ci
    #[command()]
    Gitlab(GitlabArgs),
}
