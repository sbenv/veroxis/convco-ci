pub mod normalize;

use anyhow::Result;
use clap::Parser;

use self::normalize::NormalizeArgs;

#[derive(Debug, Parser)]
pub struct SemverArgs {
    #[command(subcommand)]
    pub sub_command: SemverCommands,
}

#[derive(Parser, Debug)]
#[command()]
pub enum SemverCommands {
    /// validates that a given version is semver compliant and prints a normalized version if it starts with 'v'
    #[command()]
    Normalize(NormalizeArgs),
}

pub async fn run(args: SemverArgs) -> Result<()> {
    match args.sub_command {
        SemverCommands::Normalize(args) => normalize::run(args).await?,
    }
    Ok(())
}
