use anyhow::anyhow;
use anyhow::Result;
use clap::Parser;

#[derive(Debug, Parser)]
pub struct NormalizeArgs {
    #[arg()]
    pub version: String,
}

pub async fn run(args: NormalizeArgs) -> Result<()> {
    let version = get_normalized_semver_version(args.version.as_str())?;
    println!("{version}");
    Ok(())
}

pub fn get_normalized_semver_version(version: &str) -> Result<String> {
    let mut version = version.to_owned();
    if version.starts_with('v') {
        version = version
            .strip_prefix('v')
            .ok_or_else(|| anyhow!("failed to strip prefix 'v'"))?
            .to_string()
    }
    Ok(semver::Version::parse(version.as_str())?.to_string())
}
