use std::process::ExitCode;

#[tokio::main(flavor = "current_thread")]
async fn main() -> ExitCode {
    init();
    match convco_ci::run().await {
        Ok(_) => ExitCode::from(0),
        Err(err) => {
            tracing::error!("{err}");
            ExitCode::from(1)
        }
    }
}

fn init() {
    color_eyre::install().unwrap();

    #[cfg(debug_assertions)]
    {
        if std::env::var("RUST_LIB_BACKTRACE").is_err() {
            std::env::set_var("RUST_LIB_BACKTRACE", "1")
        }
        if std::env::var("RUST_BACKTRACE").is_err() {
            std::env::set_var("RUST_BACKTRACE", "full")
        }
    }

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info")
    }

    tracing_subscriber::fmt::fmt()
        .with_writer(std::io::stderr)
        .init();
}
