pub mod cmd;

use anyhow::Result;
use clap::Parser;
use cmd::ConvcoCiArguments;

pub async fn run() -> Result<()> {
    let args = ConvcoCiArguments::parse();
    match args.sub_command {
        cmd::ConvcoCiCommands::Git(args) => self::cmd::git::run(args).await?,
        cmd::ConvcoCiCommands::Semver(args) => self::cmd::semver::run(args).await?,
        cmd::ConvcoCiCommands::Gitlab(args) => self::cmd::gitlab::run(args).await?,
    };
    Ok(())
}
